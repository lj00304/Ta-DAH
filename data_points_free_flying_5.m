function [output,mass,l,b,s,power] = data_points_free_flying_5(n, X, qf, qf_base, basemass,payloadmass, reacherror)
h=1;
X3L = (X(3)-(reacherror/3));
X3U = (X(3)+(reacherror/3));

X4L = (X(4)-(reacherror/3));
X4U = (X(4)+(reacherror/3));

X5L = (X(5)-(reacherror/3));
X5U = (X(5)+(reacherror/3));

if X5L<0;
    X5L = 0.05;
end

if X4L<0;
    X4L = 0.05;
end

if X3L<0;
    X3L = 0.05;
end
% this upper boundary dictates the maximum possible size of the links 
for j = X5L:0.05:X5U;

for p = X4L:0.05:X4U;

for g = X3L:(0.05):X3U;

rho=2710;
radius=[0.03 0.02];

ee=[0.0378 0.0376 0.0376; 
    0.0376 0.0379 0.0376; 
    0.0376 0.0376 0.038];

basedimensions=[0.3 0.3 0.3]; 

X(2)=X(2);
X(3)=g;
X(4) = p;
X(5) = j;
linklengths=X;

i=1;
mass=zeros(1,n+1);
%determine the mass, l, s, and b matrix based on desired link lengths. 
    for i=1:n;
        mass(1,i+1)=(rho*pi*(0.03^2)*X(i))-(rho*pi*(0.02^2)*X(i));
    end
    mass(1,1)=basemass;
    
    for i=1:n;
        l(i+1,:)=[X(i) 0 0];
    end
    l(1,:)=[0 0 0];


    for i=1:n;
        s(i+1,:)=[X(i)/2 0 0];
    end
    s(1,:)=[0 0 basedimensions(3)/2];

    
    for i=1:n;
        b(i+1,:)=[X(i)/2 0 0];
    end
    b(1,:)=[0 0 0];

% run dynamic model in simulink
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
try
    options = simset('SrcWorkspace','current');
    T = sim('dynamicmodel5opp_prestabilized',[],options);
    set_param('dynamicmodel5opp_prestabilized','AlgebraicLoopSolver','LineSearch');
    warning off
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % determine the overall linear force required for operation.
    t1=sum(forces.data(:,1));
    t2=sum(forces.data(:,2));
    t3=sum(forces.data(:,3));
    XX_a=t1+t2+t3;

    % determine the overall angular force required for operation.
    t4=sum(forces.data(:,4));
    t5=sum(forces.data(:,5));
    t6=sum(forces.data(:,6));
    XX_b=t4+t5+t6;

    %determine the torque at each joint for the entire motion. 
    t7=sum(forces.data(:,7));
    t8=sum(forces.data(:,8));
    t9=sum(forces.data(:,9));
    t10=sum(forces.data(:,10));
    t11=sum(forces.data(:,11));
    XX_c=t7+t8+t9+t10+t11;

    %determine the final error 
    angend=(ang.data(end,:));
    linend=(lin.data(end,:));
    thetaend=(theta.data(end,:));

    %calculate error along the trajectory
    error = sqrt(((x_a.data-x_d.data).^2)+((y_a.data-y_d.data).^2)+((z_a.data-z_d.data).^2));
    E = max(abs(error));

    %calculate manipulability in final position
    theta = sym('theta',[5 1]);
    [mman] = manipulability5(theta,5,s,b);
    man = double(subs(mman,theta.',thetaend));

    %determine reach
    reach = X(1)+ X(2)+ X(3)+ X(4);

    %assign link lengths to the output
    output(h,1) = X(1);
    output(h,2) = X(2);
    output(h,3) = X(3);
    output(h,4) = X(4);
    output(h,5) = X(5);

    %gain 1 is for size

    output(h,6) = E;
    output(h,7) = -man;
    output(h,8) = -reach;
    output(h,9) = abs(XX_a);
    output(h,10) = abs(XX_b);
    output(h,11) = abs(XX_c);
    h=h+1;
catch
    h=h+1;
end
end
end
end

