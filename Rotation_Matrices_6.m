% This code computes the rotation matrices 

% Base_Rot_B are rotation matrices wrt the base frame
% Base_Rot are rotation matrices wrt the main reference frame

% Author: Asma Seddaoui
%--------------------------------------------------------------------------

%%% first rotation matricies defines the rotation in relation to the
%%% previous joint. 

function [Rw,Base_Rot,Rot_b,Rot_b_B] = Rotation_Matrices_6(phi,theta)


Rw=[1 0 -sin(phi(2));
    0 cos(phi(1)) sin(phi(1))*cos(phi(2));
    0 -sin(phi(1)) cos(phi(1))*cos(phi(2))]; % Based on a 321 rotation

Base_Rot=[cos(phi(1)) -sin(phi(1)) 0; % Rotation of base
    sin(phi(1)) cos(phi(1)) 0;
    0 0 1]*[cos(phi(2)) 0 sin(phi(2));
    0 1 0;
    -sin(phi(2)) 0 cos(phi(2))]*[1 0 0;
    0 cos(phi(3)) -sin(phi(3));
    0 sin(phi(3)) cos(phi(3))]; % 321 Euler angles
Base_Rot_B=eye(3,3);

%joint 1 rotates about the z axis of the system
Rot{1}=[1 0 0; 0 1 0; 0 0 1]*[cos(theta(1)) -sin(theta(1)) 0;
                              sin(theta(1)) cos(theta(1)) 0;
                              0 0 1];

Rot_b{1}=Base_Rot*Rot{1}; % wrt inertia
Rot_b_B{1}=Base_Rot_B*Rot{1};%wrt base

%joint 2 rotates about the 'x axis' of the system (its own z axis)
Rot{2}=[0 0 1; 0 1 0; -1 0 1]*[cos(theta(2)) -sin(theta(2)) 0;
                              sin(theta(2)) cos(theta(2)) 0;
                              0 0 1];
Rot_b{2}=Rot_b{1}*Rot{2}; % wrt inertia
Rot_b_B{2}=Rot_b_B{1}*Rot{2};%wrt base

%joint 3 rotates about axis parallel to the previous
Rot{3}=[1 0 0; 0 1 0; 0 0 1]*[cos(theta(3)) -sin(theta(3)) 0;
                              sin(theta(3)) cos(theta(3)) 0;
                              0 0 1];
Rot_b{3}=Rot_b{2}*Rot{3}; % wrt inertia
Rot_b_B{3}=Rot_b_B{2}*Rot{3};%wrt base

%joint 4 rotates about the same axis as joint 1 
Rot{4}=[1 0 0; 0 1 0; 0 0 1]*[cos(theta(4)) -sin(theta(4)) 0;
                              sin(theta(4)) cos(theta(4)) 0;
                              0 0 1];
Rot_b{4}=Rot_b{3}*Rot{4}; % wrt inertia
Rot_b_B{4}=Rot_b_B{3}*Rot{4}; %wrt base

%joint 5 rotates about the same axis as previous
Rot{5}=[1 0 0; 0 1 0; 0 0 1]*[cos(theta(5)) -sin(theta(5)) 0;
                              sin(theta(5)) cos(theta(5)) 0;
                              0 0 1];
Rot_b{5}=Rot_b{4}*Rot{5}; % wrt inertia
Rot_b_B{5}=Rot_b_B{4}*Rot{5}; %wrt base

%joint 6 rotates about same axis as joint 1 
Rot{6}=[0 0 -1; 0 1 0; 1 0 -1]*[cos(theta(6)) -sin(theta(6)) 0;
                              sin(theta(6)) cos(theta(6)) 0;
                              0 0 1];
Rot_b{6}=Rot_b{5}*Rot{6}; % wrt inertia
Rot_b_B{6}=Rot_b_B{5}*Rot{6}; %wrt base
end


