function [F,ff] = fuelconsumption(linearforces,Isp,TT)
f = 0;
ff = zeros(TT,3);
for i=1:1:TT
    fx = abs(linearforces.data(i,1))/Isp;
    fy = abs(linearforces.data(i,2))/Isp;
    fz = abs(linearforces.data(i,3))/Isp;
    
    ff(i,1)=fx;
    ff(i,2)=fy;
    ff(i,3)=fz;
    
    F = f + fx+fy+fz;
    f = F;
end
end

