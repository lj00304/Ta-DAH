function [Jv_m,Jw_m] = Jacobian_Matrices_For_Lucy(n,s,b,theta,Rot_b_B,Rw,Base_Rot)

%% Manipulator Dynamic Jacobian
%--------------------------------------------------------------------------
Jv_m=cell(1,n);
diffR_M_l1=cell(1,n);
SumR_M_l1=cell(1,n+1);
SumR_M_l1{1}=sym(zeros(3,1));

diffR_M_l2=cell(1,n);
SumR_M_l2=cell(1,n+1);
SumR_M_l2{1}=sym(zeros(3,1));
%---
SumR_Manip_l1=cell(1,n);
SumR_Manip_l2=cell(1,n);
for i=1:n
    SumR_Manip_l1{i}=sym(zeros(3,1));
    SumR_Manip_l2{i}=sym(zeros(3,1));
end

SumR_Manip_l=cell(1,n);
for i=1:n
    SumR_Manip_l{i}=sym(zeros(3,n));
end

for i=1:n
    for j=1:i 
        if j==1
            for k=1:j
                diffR_M_l1{k}=(diff(Rot_b_B{j},theta(k)))*b(j+1,:).';
                SumR_M_l1{k+1}=SumR_M_l1{k}+diffR_M_l1{k};
                SumR_Manip_l1{k}=SumR_M_l1{1,k+1};
                        
                diffR_M_l2{k}=0; % The sum become from k to 0, which is not possible
                SumR_M_l2{k+1}=SumR_M_l2{k}+diffR_M_l2{k};
                SumR_Manip_l2{k}=SumR_M_l2{1,k+1};
            end
        else
            for k=1:j
                diffR_M_l1{k}=(diff(Rot_b_B{j},theta(k)))*b(j+1,:).';
                SumR_M_l1{k+1}=SumR_M_l1{k}+diffR_M_l1{k};
                SumR_Manip_l1{k}=SumR_M_l1{k+1};
            end
            %
            for k=1:j-1
                diffR_M_l2{k}=(diff(Rot_b_B{j-1},theta(k)))*s(j,:).';
                SumR_M_l2{k+1}=SumR_M_l2{k}+diffR_M_l2{k};
                SumR_Manip_l2{k}=SumR_M_l2{k+1};
            end
        end
            SumR1=[SumR_Manip_l1{:}];
            SumR2=[SumR_Manip_l2{:}];
            SumR_Manip_l{i}=SumR1+SumR2;
    end 
end

for i=1:n
    Jv_m{i}=SumR_Manip_l{i};
end
%% Rotational Jacobian Computation 
Jw_sc_inter=cell(1,3);
for i=1:3
    Jw_sc_inter{i}=sym(zeros(3,3));
end
Jw_sc=cell(1,3);
for i=1:3
    Jw_sc{i}=sym(zeros(3,6));
end
Jw_m=cell(1,n);
for i=1:n
    Jw_m{i}=sym(zeros(3,n));
end

Jw_sc_inter{1}(:,1)=Rw*[1;0;0];
Jw_sc_inter{2}(:,2)=Rw*[0;1;0];
Jw_sc_inter{3}(:,3)=Rw*[0;0;1];

for i=1:3
    Jw_sc{i}=[zeros(3,3),Jw_sc_inter{i}];
end
% Manipulator rotational Jacobian -----------

for i=1:n
    for j=1:i
        if j==1
           Jw_m{i}(:,j)=Base_Rot*[0;0;1]; % ASMA MAKE SURE IT'S BASE_ROT YOU HAVE TO USE
        else
           Jw_m{i}(:,j)=Rot_b_B{j-1}*[0;0;1];
        end
    end
end



