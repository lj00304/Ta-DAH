function [P] = Power_f(Td,Te,a1,a2)
eff=0.294;
Id=0.8;
sun_rad=1367;

Xe=0.6;
Xd=0.8;

angle_1=0;
angle_2=degtorad(45);

prob_1=1/8;
prob_2=0.5;
prob_3=0.25;

P1=eff*Id*a1*sun_rad*cos(angle_1);
P2=eff*Id*a2*sun_rad*cos(angle_2);
P3=eff*Id*a2*sun_rad*cos(angle_1);

Ptot=(P1*prob_1)+(P2*prob_2*2)+(P3*prob_3);
P=(Ptot*Xd*Xe*Td)/((Xe*Td)+(Xd*Te));

end