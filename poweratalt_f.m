function [Powerf] = poweratalt_f(alt)

pannelarea=0.003018;
Re=6371;
mu=3.98601*(10^5);
A=35786+Re;
T=(2*pi*sqrt(A^3/mu));

alpha=acos(Re/(Re+A));

e=(pi-(2*alpha))/(2*pi);
d=(pi+(2*alpha))/(2*pi);

Te=T*e;
Td=T*d;

area_3u_1=pannelarea*6;
area_3u_2=pannelarea*6;

area_6u_1=pannelarea*6;
area_6u_2=pannelarea*12;

area_12u_1=pannelarea*12;
area_12u_2=pannelarea*12;

area_18u_1=pannelarea*12;
area_18u_2=pannelarea*18;

area_24u_1=pannelarea*12;
area_24u_2=pannelarea*24;

area_27u_1=pannelarea*18;
area_27u_2=pannelarea*18;

dimensions{1} = [0.3 0.2 0.2 3 12 17.5];
dimensions{2} = [0.3 0.3 0.2 4 18 25.4];
dimensions{3} = [0.3 0.4 0.2 5 241 33.4];
dimensions{4} = [0.3 0.3 0.3 6 271 37.4];
dimensions{5} = [0.3 0.4 0.2 7 242 43.4];
dimensions{6} = [0.3 0.3 0.3 8 272 47.5];  
dimensions{7} = [0.4 0.4 0.4 9 64 65];  
dimensions{9} = [0.5 0.5 0.5 10 125 85];  
dimensions{10} = [0.6 0.6 0.6 11 216 100];  


area_64u_1=pannelarea*32;
area_64u_2=pannelarea*32;

area_125u_1=pannelarea*50;
area_125u_2=pannelarea*50;

area_216u_1=pannelarea*72;
area_216u_2=pannelarea*72;

P3U=Power_f(Td,Te,area_3u_1,area_3u_2);
P6U=Power_f(Td,Te,area_6u_1,area_6u_2);
P12U=Power_f(Td,Te,area_12u_1,area_12u_2);
P18U=Power_f(Td,Te,area_18u_1,area_18u_2);
P24U=Power_f(Td,Te,area_24u_1,area_24u_2);
P27U=Power_f(Td,Te,area_27u_1,area_27u_2)*100000000000;
P64U=Power_f(Td,Te,area_64u_1,area_64u_2);
P125U=Power_f(Td,Te,area_125u_1,area_125u_2);
P216U=Power_f(Td,Te,area_216u_1,area_216u_2);


Powerf = [P3U P6U P12U P18U P24U P27U P24U P27U P64U P125U P216U];
end

