clear all
clc 
% Opens prompt window that allows the user to input the desired parameters
% for optimisation.
Title = 'Space Robot Optimizer';
Options.Resize = 'on';
Options.Interpreter = 'tex';
Options.ApplyButton = 'off';
Options.ButtonNames = {'Optimise','Cancel'}; %<- default names, included here just for illustration
Option.Dim = 5; % Horizontal dimension in fields

beep off

Prompt = {};
Formats = {};
DefAns = struct([]);

Prompt(1,:) = {['This toolbox will produce the optimised design for a small space robot' ... 
    ' including manipulator and base spacecraft size. Any options left blank will be filled with the standard options.'],[],[]};
Formats(1,1).type = 'text';
Formats(1,1).size = [-1 0];
Formats(1,1).span = [1 2];

Prompt(2,:) = {'Desired Reach (m)', 'reach',[]};
Formats(2,1).type = 'edit';
Formats(2,1).format = 'float';
Formats(2,1).limits = [0 1]
Formats(2,1).size = 60; 
DefAns(1).reach = 0.7;

Prompt(3,:) = {'Maximum Payload Mass (kg):', 'payloadmass',[]};
Formats(2,2).type = 'edit';
Formats(2,2).format = 'integer';
Formats(2,2).limits = [0 100]; 
Formats(2,2).size = 40;
Formats(2,2).unitsloc = 'bottomleft';
DefAns.payloadmass = 0;

Prompt(4,:) = {'+/-(m)', 'reacherror',[]};
Formats(3,1).type = 'edit';
Formats(3,1).format = 'float';
Formats(3,1).limits = [0.05 0.5]
Formats(3,1).size = 60; 
DefAns(1).reacherror = 0.3;

Prompt(5,:) = {'Desired Degrees of Freedom','n',[]};
Formats(4,1).type = 'list';
Formats(4,1).format = 'text';
Formats(4,1).style = 'togglebutton';
Formats(4,1).items = {'5 DoF' '6 DoF' '7 DoF'};
DefAns.n = '5 Dof';%3; % yen

Prompt(6,:) = {'Cost Function Weighting','weight',[]};
Formats(5,1).type = 'edit';
Formats(5,1).format = 'text';
Formats(5,1).size = 300
DefAns.weight = {'0 0 0 0 0'};

Prompt(7,:) = {'Enter Finishing State (deg):','state',[]};
Formats(6,1).type = 'edit';
Formats(6,1).format = 'text';
Formats(6,1).size = 300
DefAns.state = {'50 -50 50 0 0 0 0 0 0 0 0 0 0'}

[Answer,Cancelled] = inputsdlg(Prompt,Title,Formats,DefAns,Options)

qout = str2num(Answer.state{1});

if Cancelled == 1
    h = msgbox({'No optimisation carried out'})
elseif Cancelled == 0

% All user inputs are assigned to the desired variables
desiredreach = Answer.reach;
reacherror = Answer.reacherror;
payloadmass = Answer.payloadmass;

%assigned DoF is one less than user input due to the action of the end
%effector
if Answer.n(1) == '5'
    n = 4;
elseif Answer.n(1) == '6'
    n = 5;
elseif Answer.n(1) == '7'
    n = 6;
end

% non desired parameters are set to zero as an exhaustive search has
% already been carried out that considers all options. 
% 20 is maximise performance 

% runs through free-flying and free-floating options for 4 DoF
if n == 4
    [X, F, basedimensions, Operatingtime, P_forformfactor, forces, lin, ang, theta, vv, RW, P_subsystems, x_a, y_a, z_a, x_d, y_d, z_d, power] = freeflying4(desiredreach, payloadmass, n, reacherror, qout);
    
    %plot the satellite and arm with trajectory
    z = basedimensions(1);
    y = basedimensions(2);
    x = basedimensions(3);
    
    l2=X(2);
    l3=X(3);

    %initialise the angular and linear positions of the base spacecraft 
    angstart=ang.data(1,:);
    alpha=angstart(1);
    beta=angstart(2);
    gamma=angstart(3);

    linstart=lin.data(1,:);
    xx=linstart(1);
    yy=linstart(2);
    zz=linstart(3);

    th=theta.data(1,:);
    a=[-x/2;y/2;z/2];
    b=[-x/2;y/2;-z/2];
    c=[x/2;y/2;-z/2];
    d=[x/2;y/2;z/2];
    e=[(x/2);(-y/2);(z/2)];
    f=[(x/2);(-y/2);(-z/2)];
    g=[(-x/2);(-y/2);(z/2)];
    h=[(-x/2);(-y/2);(-z/2)];

    Rx=[1 0 0;0 cos(alpha) -sin(alpha); 0 sin(alpha) cos(alpha)];
    Ry=[cos(beta) 0 sin(beta); 0 1 0; -sin(beta) 0 cos(beta)];
    Rz=[cos(gamma) -sin(gamma) 0; sin(gamma) cos(gamma) 0; 0 0 1];
    Rt=[xx;yy;zz];
    R1=Rx*Ry*Rz;

    a=(R1*a)+Rt;
    b=(R1*b)+Rt;
    c=(R1*c)+Rt;
    d=(R1*d)+Rt;
    e=(R1*e)+Rt;
    f=(R1*f)+Rt;
    g=(R1*g)+Rt;
    h=(R1*h)+Rt;

    cube1=[a.';b.';c.';d.';a.'];
    cube2=[d.';e.';f.';c.';d.'];
    cube3=[e.';g.';h.';f.';e.'];
    cube4=[a.';g.';h.';b.';a.'];
    
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(2,2,2)
    h1 = plot3(cube1(:,1),cube1(:,2),cube1(:,3),'k-')
    hold on
    plot3(cube2(:,1),cube2(:,2),cube2(:,3),'k-')
    hold on
    plot3(cube3(:,1),cube3(:,2),cube3(:,3),'k-')
    hold on
    plot3(cube4(:,1),cube4(:,2),cube4(:,3),'k-')
    axis('equal')
    set(gca,'fontsize',14)

    basepoint=(R1*[0;0;z/2])+Rt;
    plot3(basepoint(1),basepoint(2),basepoint(3),'k.','MarkerSize',15)
    hold on 

    R2=[cos(th(1)) -sin(th(1)) 0;
        sin(th(1)) cos(th(1)) 0;
        0 0 1]*R1;

    joint2=(basepoint-Rt)+[l2*sin(th(2)); 0; (l2*cos(th(2)))];
    J2=(R2*joint2)+Rt;
 
    joint3=[l3*sin(th(2)+th(3)); 0;l3*cos(th(2)+th(3))];
    J3=R2*(joint2+joint3)+Rt;

    plot3(J2(1),J2(2),J2(3),'k.','MarkerSize',15)
    hold on
    plot3(J3(1),J3(2),J3(3),'k.','MarkerSize',15)
    hold on
    
    i=[basepoint.';J2.'];
    j=[J2.';J3.'];

    plot3(i(:,1),i(:,2),i(:,3),'k')
    hold on
    plot3(j(:,1),j(:,2),j(:,3),'k')
    hold on
    
    %define the final base position
    angend1=ang.data(end,:)
    alpha=angend1(1);
    beta=angend1(2);
    gamma=angend1(3);

    linend1=lin.data(end,:)
    xx=linend1(1);
    yy=linend1(2);
    zz=linend1(3);

    th=theta.data(end,:);

    Rx=[1 0 0;0 cos(alpha) -sin(alpha); 0 sin(alpha) cos(alpha)];
    Ry=[cos(beta) 0 sin(beta); 0 1 0; -sin(beta) 0 cos(beta)];
    Rz=[cos(gamma) -sin(gamma) 0; sin(gamma) cos(gamma) 0; 0 0 1];
    Rt=[xx;yy;zz];
    R1=Rx*Ry*Rz;

    a=[-x/2;y/2;z/2];
    b=[-x/2;y/2;-z/2];
    c=[x/2;y/2;-z/2];
    d=[x/2;y/2;z/2];
    e=[(x/2);(-y/2);(z/2)];
    f=[(x/2);(-y/2);(-z/2)];
    g=[(-x/2);(-y/2);(z/2)];
    h=[(-x/2);(-y/2);(-z/2)];

    a=(R1*a)+Rt;
    b=(R1*b)+Rt;
    c=(R1*c)+Rt;
    d=(R1*d)+Rt;
    e=(R1*e)+Rt;
    f=(R1*f)+Rt;
    g=(R1*g)+Rt;
    h=(R1*h)+Rt;

    cube1=[a.';b.';c.';d.';a.'];
    cube2=[d.';e.';f.';c.';d.'];
    cube3=[e.';g.';h.';f.';e.'];
    cube4=[a.';g.';h.';b.';a.'];

    plot3(cube1(:,1),cube1(:,2),cube1(:,3),'b-')
    hold on
    plot3(cube2(:,1),cube2(:,2),cube2(:,3),'b-')
    hold on
    plot3(cube3(:,1),cube3(:,2),cube3(:,3),'b-')
    hold on
    plot3(cube4(:,1),cube4(:,2),cube4(:,3),'b-')
    xlabel('y axis (inertial frame)','fontsize',12)
    ylabel('x axis (inertial frame)','fontsize',12)
    zlabel('z axis (inertial frame)','fontsize',12)
    
    th=theta.data(end,:);
    
    basepoint=(R1*[0;0;z/2])+Rt;
    plot3(basepoint(1),basepoint(2),basepoint(3),'k.','MarkerSize',15)
    hold on 
    
    R2=[cos(th(1)) -sin(th(1)) 0;
    sin(th(1)) cos(th(1)) 0;
    0 0 1]*R1;
    
    joint2=(basepoint-Rt)+[l2*sin(th(2)); 0; (l2*cos(th(2)))];
    J2=(R2*joint2)+Rt;
 
    joint3=[l3*sin(th(2)+th(3)); 0;l3*cos(th(2)+th(3))];
    J3=R2*(joint2+joint3)+Rt;

    plot3(J2(1),J2(2),J2(3),'b.','MarkerSize',15)
    hold on
    plot3(J3(1),J3(2),J3(3),'b.','MarkerSize',15)
    hold on

    i=[basepoint.';J2.'];
    j=[J2.';J3.'];

    plot3(x_a.data, y_a.data, z_a.data,'b--')
    hold on
    plot3(x_d.data, y_d.data, z_d.data,'k--')
    hold on
    
    h2 = plot3(i(:,1),i(:,2),i(:,3),'b')
    hold on
    plot3(j(:,1),j(:,2),j(:,3),'b')
    hold on
    grid on
    title('Pre and Post Manvouver Position','fontsize',16)
    legend([h1 h2],{'Original Position','Final Position'},'fontsize',12)
    
    figure(1)
    subplot(2,2,3)
    [Xx,Yy,Zz] = workspace_plot4(X)
    plot3(Xx, Yy, Zz, '.')
    title('Workspace Representation','fontsize',16)
    xlabel('y axis (inertial frame)','fontsize',12)
    ylabel('x axis (inertial frame)','fontsize',12)
    zlabel('z axis (inertial frame)','fontsize',12)
    grid on
    hold on
    
    figure(1)
    subplot(2,2,4)
    armconfig4(basedimensions(1:3), X)
    title('Arm Schematic','fontsize',16)

    % outputs important parameters in table
    x = nan;
    h={'Link Lengths (m)(1,2,end effector)' 'Optimal formfactor' 'Base Dimensions (m)' 'Base Mass(kg) (wet)' 'Base Mass (kg) (dry)' 'Fuel Consumption: per manouver (kg)' 'Power Consumption: per manouver (W)'}
    data = [X(2) X(3) X(4);
    basedimensions(5) x x; 
    basedimensions(1) basedimensions(2) basedimensions(3);
    basedimensions(6) x x; 
    basedimensions(6)-1.5 x x;
    F x x;
    P_subsystems x x;]
    t=uitable(figure(1),'Position',[200 650 680 180],'rowname',h,'data',data)
    txt_title = uicontrol('Style', 'text', 'Position', [200 810 680 30], 'String', 'Space Robot Parameters','fontsize',16);
    pause(2)
    set(t,'ColumnWidth',{80})
    
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(3,2,1)
    plot(abs(forces.data(:,7:end)))
    xlim([0,200])
    title('Torque Required at Each Joint')
    legend('Joint 1','Joint 2','Joint 3','Joint 4') 
    xlabel('Time')
    ylabel('Torque (Nm)')
    
    subplot(3,2,3)
    plot(power.data(:,7:end))
    set(gca,'FontSize',15,'fontWeight','bold')
    xlim([0,200])
    title('Mechanical Power Required at Each Joint')
    xlabel('Time (s)')
    ylabel('Power (W)')
    legend('Joint 1','Joint 2','Joint 3','Joint 4') 
    hold on
 
    subplot(3,2,5)
    plot(power.data(:,7:end)/0.6,'linewidth',5)
    set(gca,'FontSize',15,'fontWeight','bold')
    xlim([0,200])
    % title('Electrical Power Required at Each Joint (assuming 60% motor efficiency)')
    xlabel('Time')
    ylabel('Power (W)')
    legend('Joint 1','Joint 2','Joint 3','Joint 4') 
    hold on
    
    subplot(3,2,6)
    plot(theta.data)
    xlim([0,200])
    title('Joint Position Throughout Manouver')
    xlabel('Time')
    ylabel('Position (rad)')
    legend('Joint 1','Joint 2','Joint 3','Joint 4') 
    hold on
    
    x = nan;
    
    h={'Peak Torque (Nm)' 'Average Torque (Nm)' 'Weight (kg) <'}
    data = [max(max(abs(forces.data(:,7:end)))); mean(mean(abs(forces.data(:,7:end)))); 1]
    t=uitable(figure(2),'Position',[1200 700 300 100],'rowname',h,'data',data)
    txt_title = uicontrol('Style', 'text', 'Position', [1250 810 200 30], 'String', 'Motor Requirements','fontsize',16);
    pause(2)
    set(t,'ColumnWidth',{80})
    
    h={'Workspace Area (m^2)' 'Reach (m)' 'Max Payload Dimension (m)' 'Link Diameter: inner(m)','Link Diameter: outer(m)'}
    [area] = workspacearea(X,n)
    data = [area; X(1)+X(2)+X(3)+X(4); 0.8; 0.03; 0.02]
    t=uitable(figure(2),'Position',[1150 450 400 150],'rowname',h,'data',data)
    txt_title = uicontrol('Style', 'text', 'Position', [1300 600 100 30], 'String', 'Arm Spec','fontsize',16);
    pause(2)
    set(t,'ColumnWidth',{80})
    
    for i=1:n-1
        if theta.data(195,i)-theta.data(200,i)>0.0005
           annotation('textbox', [0.6, 0.4, 0.1, 0.1], 'String', "System is unstable in this configuration")
        m = 0
        end
    end
        
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(3,2,1)
    plot(lin.data)
    xlim([0,200])
    title('Linear Position of Base','fontsize',12)
    xlabel('Time')
    ylabel('Linear Position (m)')
    legend('x axis','y axis','z axis') 
    
    subplot(3,2,2)
    plot(ang.data)
    xlim([0,200])
    title('Angular Position of Base','fontsize',12)
    xlabel('Time')
    ylabel('Angular Position (rad)')
    legend('x axis','y axis','z axis')
    
    subplot(3,2,3)
    plot(forces.data(:,1:3))
    xlim([0,200])
    title('Force From Thrusters','fontsize',12)
    xlabel('Time')
    ylabel('Linear Force (N)')
    legend('x axis','y axis','z axis') 

    subplot(3,2,4)
    plot(forces.data(:,4:6),'linewidth',5)
    set(gca,'FontSize',15,'fontWeight','bold')
    xlim([0,200])
    title('Torque From Reaction Wheels','fontsize',12)
    xlabel('Time (s)')
    ylabel('Torque (Nm)')
    legend('x axis','y axis','z axis')
    
    subplot(3,2,5)
    plot(vv)
    xlim([0,200])
    title('Reaction Wheel Velocity','fontsize',12)
    xlabel('Time (s)')
    ylabel('Speed of Reaction Wheel (R.P.M)')
    legend('x axis','y axis','z axis') 
    
    if RW == 1
        annotation('textbox', [0.15, 0.15, 0.1, 0.1], 'String', "RW used is Clyde Space small sat reaction wheel")
    elseif RW == 2
        annotation('textbox', [0.15, 0.15, 0.1, 0.1], 'String', "RW used is Collins aerospace reaction wheel")
    end
    
    subplot(3,2,6)
    bar(abs(forces.data(:,1:3)))
    xlim([0,200])
    title('Thruster Firing Pattern','fontsize',12)
    xlabel('Time (s)')
    ylabel('Mass of Fuel Ejected (kg)')
    legend('x axis','y axis','z axis')
    
        
elseif n == 5
    [X, F, basedimensions, Operatingtime, P_forformfactor, forces, lin, ang, theta, vv, RW, P_subsystems, x_a, y_a, z_a, x_d, y_d, z_d, power] = freeflying5(desiredreach, payloadmass, n, reacherror, qout);
    
    %plot the satellite and arm with trajectory
    z = basedimensions(1);
    y = basedimensions(2);
    x = basedimensions(3);
    
    l2=X(2);
    l3=X(3);
    l4=X(4);

    %initialise the angular and linear positions of the base spacecraft 
    angstart=ang.data(1,:);
    alpha=angstart(1);
    beta=angstart(2);
    gamma=angstart(3);

    linstart=lin.data(1,:);
    xx=linstart(1);
    yy=linstart(2);
    zz=linstart(3);

    th=theta.data(1,:);
    a=[-x/2;y/2;z/2];
    b=[-x/2;y/2;-z/2];
    c=[x/2;y/2;-z/2];
    d=[x/2;y/2;z/2];
    e=[(x/2);(-y/2);(z/2)];
    f=[(x/2);(-y/2);(-z/2)];
    g=[(-x/2);(-y/2);(z/2)];
    h=[(-x/2);(-y/2);(-z/2)];

    Rx=[1 0 0;0 cos(alpha) -sin(alpha); 0 sin(alpha) cos(alpha)];
    Ry=[cos(beta) 0 sin(beta); 0 1 0; -sin(beta) 0 cos(beta)];
    Rz=[cos(gamma) -sin(gamma) 0; sin(gamma) cos(gamma) 0; 0 0 1];
    Rt=[xx;yy;zz];
    R1=Rx*Ry*Rz;

    a=(R1*a)+Rt;
    b=(R1*b)+Rt;
    c=(R1*c)+Rt;
    d=(R1*d)+Rt;
    e=(R1*e)+Rt;
    f=(R1*f)+Rt;
    g=(R1*g)+Rt;
    h=(R1*h)+Rt;

    cube1=[a.';b.';c.';d.';a.'];
    cube2=[d.';e.';f.';c.';d.'];
    cube3=[e.';g.';h.';f.';e.'];
    cube4=[a.';g.';h.';b.';a.'];
    
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(2,2,2)
    h1 = plot3(cube1(:,1),cube1(:,2),cube1(:,3),'k-')
    hold on
    plot3(cube2(:,1),cube2(:,2),cube2(:,3),'k-')
    hold on
    plot3(cube3(:,1),cube3(:,2),cube3(:,3),'k-')
    hold on
    plot3(cube4(:,1),cube4(:,2),cube4(:,3),'k-')
    axis('equal')
    set(gca,'fontsize',14)

    basepoint=(R1*[0;0;z/2])+Rt;
    plot3(basepoint(1),basepoint(2),basepoint(3),'k.','MarkerSize',15)
    hold on 

    R2=[cos(th(1)) -sin(th(1)) 0;
        sin(th(1)) cos(th(1)) 0;
        0 0 1]*R1;

    joint2=(basepoint-Rt)+[l2*sin(th(2)); 0; (l2*cos(th(2)))];
    J2=(R2*joint2)+Rt;
 
    joint3=[l3*sin(th(2)+th(3)); 0;l3*cos(th(2)+th(3))];
    J3=R2*(joint2+joint3)+Rt;

    joint4=[l4*sin(th(2)+th(3)+th(4)); 0;l4*cos(th(2)+th(3)+th(4))];
    J4=R2*(joint2+joint3+joint4)+Rt;
    
    plot3(J2(1),J2(2),J2(3),'k.','MarkerSize',15)
    hold on
    plot3(J3(1),J3(2),J3(3),'k.','MarkerSize',15)
    hold on
    plot3(J4(1),J4(2),J4(3),'k.','MarkerSize',15)
    hold on
    
    i=[basepoint.';J2.'];
    j=[J2.';J3.'];
    k=[J3.';J4.'];

    plot3(i(:,1),i(:,2),i(:,3),'k')
    hold on
    plot3(j(:,1),j(:,2),j(:,3),'k')
    hold on
    plot3(k(:,1),k(:,2),k(:,3),'k')
    
    %define the final base position
    angend1=ang.data(end,:)
    alpha=angend1(1);
    beta=angend1(2);
    gamma=angend1(3);

    linend1=lin.data(end,:)
    xx=linend1(1);
    yy=linend1(2);
    zz=linend1(3);

    th=theta.data(end,:);

    Rx=[1 0 0;0 cos(alpha) -sin(alpha); 0 sin(alpha) cos(alpha)];
    Ry=[cos(beta) 0 sin(beta); 0 1 0; -sin(beta) 0 cos(beta)];
    Rz=[cos(gamma) -sin(gamma) 0; sin(gamma) cos(gamma) 0; 0 0 1];
    Rt=[xx;yy;zz];
    R1=Rx*Ry*Rz;

    a=[-x/2;y/2;z/2];
    b=[-x/2;y/2;-z/2];
    c=[x/2;y/2;-z/2];
    d=[x/2;y/2;z/2];
    e=[(x/2);(-y/2);(z/2)];
    f=[(x/2);(-y/2);(-z/2)];
    g=[(-x/2);(-y/2);(z/2)];
    h=[(-x/2);(-y/2);(-z/2)];

    a=(R1*a)+Rt;
    b=(R1*b)+Rt;
    c=(R1*c)+Rt;
    d=(R1*d)+Rt;
    e=(R1*e)+Rt;
    f=(R1*f)+Rt;
    g=(R1*g)+Rt;
    h=(R1*h)+Rt;

    cube1=[a.';b.';c.';d.';a.'];
    cube2=[d.';e.';f.';c.';d.'];
    cube3=[e.';g.';h.';f.';e.'];
    cube4=[a.';g.';h.';b.';a.'];

    plot3(cube1(:,1),cube1(:,2),cube1(:,3),'b-')
    hold on
    plot3(cube2(:,1),cube2(:,2),cube2(:,3),'b-')
    hold on
    plot3(cube3(:,1),cube3(:,2),cube3(:,3),'b-')
    hold on
    plot3(cube4(:,1),cube4(:,2),cube4(:,3),'b-')
    xlabel('y axis (inertial frame)','fontsize',12)
    ylabel('x axis (inertial frame)','fontsize',12)
    zlabel('z axis (inertial frame)','fontsize',12)
    
    th=theta.data(end,:);
    
    basepoint=(R1*[0;0;z/2])+Rt;
    plot3(basepoint(1),basepoint(2),basepoint(3),'k.','MarkerSize',15)
    hold on 
    
    R2=[cos(th(1)) -sin(th(1)) 0;
    sin(th(1)) cos(th(1)) 0;
    0 0 1]*R1;
    
    joint2=(basepoint-Rt)+[l2*sin(th(2)); 0; (l2*cos(th(2)))];
    J2=(R2*joint2)+Rt;
 
    joint3=[l3*sin(th(2)+th(3)); 0;l3*cos(th(2)+th(3))];
    J3=R2*(joint2+joint3)+Rt;
    
    joint4=[l4*sin(th(2)+th(3)+th(4)); 0;l4*cos(th(2)+th(3)+th(4))];
    J4=R2*(joint2+joint3+joint4)+Rt;

    plot3(J2(1),J2(2),J2(3),'b.','MarkerSize',15)
    hold on
    plot3(J3(1),J3(2),J3(3),'b.','MarkerSize',15)
    hold on
    plot3(J4(1),J4(2),J4(3),'b.','MarkerSize',15)
    hold on

    i=[basepoint.';J2.'];
    j=[J2.';J3.'];
    k=[J3.';J4.'];

    plot3(x_a.data, y_a.data, z_a.data,'b--')
    hold on
    plot3(x_d.data, y_d.data, z_d.data,'k--')
    hold on
    
    h2 = plot3(i(:,1),i(:,2),i(:,3),'b')
    hold on
    plot3(j(:,1),j(:,2),j(:,3),'b')
    hold on
    plot3(k(:,1),k(:,2),k(:,3),'b')
    
    grid on
    title('Pre and Post Manvouver Position','fontsize',16)
    legend([h1 h2],{'Original Position','Final Position'},'fontsize',12)
    
    figure(1)
    subplot(2,2,3)
    [Xx,Yy,Zz] = workspace_plot5(X);
    plot3(Xx, Yy, Zz, '.')
    title('Workspace Representation','fontsize',16)
    xlabel('y axis (inertial frame)','fontsize',12)
    ylabel('x axis (inertial frame)','fontsize',12)
    zlabel('z axis (inertial frame)','fontsize',12)
    grid on
    hold on
    
    figure(1)
    subplot(2,2,4)
    armconfig5(basedimensions(1:3), X)
    title('Arm Schematic','fontsize',16)

    % outputs important parameters in table
    x = nan;
    h={'Link Lengths (m)(1,2,3,end effector)' 'Optimal formfactor' 'Base Dimensions (m)' 'Base Mass(kg) (wet)' 'Base Mass (kg) (dry)' 'Fuel Consumption: per manouver (kg)' 'Power Consumption: per manouver (W)'}
    data = [X(2) X(3) X(4) X(5);
    basedimensions(5) x x x; 
    basedimensions(1) basedimensions(2) basedimensions(3) x;
    basedimensions(6) x x x; 
    basedimensions(6)-1.5 x x x;
    F x x x;
    P_subsystems x x x;]
    t=uitable(figure(1),'Position',[100 450 700 180],'rowname',h,'data',data)
    %txt_title = uicontrol('Style', 'text', 'Position', [350 450 200 50], 'String', 'Space Robot Parameters','fontsize',16);
    pause(2)
    set(t,'ColumnWidth',{80})
    
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(3,2,1)
    plot(abs(forces.data(:,7:end)))
    xlim([0,200])
    title('Torque Required at Each Joint')
    legend('Joint 1','Joint 2','Joint 3','Joint 4','Joint 5') 
    xlabel('Time')
    ylabel('Torque (Nm)')
    
    subplot(3,2,3)
    plot(power.data(:,7:end))
    xlim([0,200])
    title('Mechanical Power Required at Each Joint')
    xlabel('Time')
    ylabel('Power (W)')
    legend('Joint 1','Joint 2','Joint 3','Joint 4','Joint 5') 
    hold on
 
    subplot(3,2,5)
    plot(power.data(:,7:end)/0.6)
    xlim([0,200])
    title('Electrical Power Required at Each Joint (assuming 60% motor efficiency)')
    xlabel('Time')
    ylabel('Power (W)')
    legend('Joint 1','Joint 2','Joint 3','Joint 4','Joint 5') 
    hold on
    
    subplot(3,2,6)
    plot(theta.data)
    xlim([0,200])
    title('Joint Position Throughout Manouver')
    xlabel('Time')
    ylabel('Position (rad)')
    legend('Joint 1','Joint 2','Joint 3','Joint 4','Joint 5') 
    hold on
    
    x = nan;
    
    h={'Peak Torque (Nm)' 'Average Torque (Nm)' 'Weight (kg) <'}
    data = [max(max(abs(forces.data(:,7:end)))); mean(mean(abs(forces.data(:,7:end)))); 1]
    t=uitable(figure(2),'Position',[900 550 300 100],'rowname',h,'data',data)
    %txt_title = uicontrol('Style', 'text', 'Position', [1150 760 200 30], 'String', 'Motor Requirements','fontsize',16);
    pause(2)
    set(t,'ColumnWidth',{80})
    
    h={'Workspace Area (m^2)' 'Reach (m)' 'Max Payload Dimension (m)' 'Link Diameter: inner(m)','Link Diameter: outer(m)'}
    [area] = workspacearea(X,n)
    data = [area; X(1)+X(2)+X(3)+X(4)+X(5); 0.8; 0.03; 0.02]
    t=uitable(figure(2),'Position',[900 350 400 150],'rowname',h,'data',data)
    %txt_title = uicontrol('Style', 'text', 'Position', [900 400 100 30], 'String', 'Arm Spec','fontsize',16);
    pause(2)
    set(t,'ColumnWidth',{80})
    
    for i=1:n-1
        if theta.data(195,i)-theta.data(200,i)>0.0005
           annotation('textbox', [0.6, 0.4, 0.1, 0.1], 'String', "System is unstable in this configuration")
        m = 0
        end
    end
    
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(3,2,1)
    plot(lin.data)
    xlim([0,200])
    title('Linear Position of Base','fontsize',12)
    xlabel('Time')
    ylabel('Linear Position (m)')
    legend('x axis','y axis','z axis') 
    
    subplot(3,2,2)
    plot(ang.data)
    xlim([0,200])
    title('Angular Position of Base','fontsize',12)
    xlabel('Time')
    ylabel('Angular Position (rad)')
    legend('x axis','y axis','z axis')
    
    subplot(3,2,3)
    plot(forces.data(:,1:3))
    xlim([0,200])
    title('Force From Thrusters','fontsize',12)
    xlabel('Time')
    ylabel('Linear Force (N)')
    legend('x axis','y axis','z axis') 

    subplot(3,2,4)
    plot(forces.data(:,4:6))
    xlim([0,200])
    title('Torque From Reaction Wheels','fontsize',12)
    xlabel('Time')
    ylabel('Torque (Nm)')
    legend('x axis','y axis','z axis')
    
    subplot(3,2,5)
    plot(vv)
    xlim([0,200])
    title('Reaction Wheel Velocity','fontsize',12)
    xlabel('Time (s)')
    ylabel('Speed of Reaction Wheel (R.P.M)')
    legend('x axis','y axis','z axis')
    
    if RW == 1
        annotation('textbox', [0.15, 0.15, 0.1, 0.1], 'String', "RW used is Clyde Space small sat reaction wheel")
    elseif RW == 2
        annotation('textbox', [0.15, 0.15, 0.1, 0.1], 'String', "RW used is Collins aerospace reaction wheel")
    end
    
    subplot(3,2,6)
    bar(abs(forces.data(:,1:3)))
    xlim([0,200])
    title('Thruster Firing Pattern','fontsize',12)
    xlabel('Time (s)')
    ylabel('Mass of Fuel Ejected (kg)')
    legend('x axis','y axis','z axis')
    
elseif n==6
    [X, F, basedimensions, Operatingtime, P_forformfactor, forces, lin, ang, theta, vv, RW, P_subsystems, x_a, y_a, z_a, x_d, y_d, z_d, power] = freeflying6(desiredreach, payloadmass, n, reacherror, qout);
    
    %plot the satellite and arm with trajectory
    z = basedimensions(1);
    y = basedimensions(2);
    x = basedimensions(3);
    
    l2=X(2);
    l3=X(3);
    l4=X(4);
    l5=X(5);

    %initialise the angular and linear positions of the base spacecraft 
    angstart=ang.data(1,:);
    alpha=angstart(1);
    beta=angstart(2);
    gamma=angstart(3);

    linstart=lin.data(1,:);
    xx=linstart(1);
    yy=linstart(2);
    zz=linstart(3);

    th=theta.data(1,:);
    a=[-x/2;y/2;z/2];
    b=[-x/2;y/2;-z/2];
    c=[x/2;y/2;-z/2];
    d=[x/2;y/2;z/2];
    e=[(x/2);(-y/2);(z/2)];
    f=[(x/2);(-y/2);(-z/2)];
    g=[(-x/2);(-y/2);(z/2)];
    h=[(-x/2);(-y/2);(-z/2)];

    Rx=[1 0 0;0 cos(alpha) -sin(alpha); 0 sin(alpha) cos(alpha)];
    Ry=[cos(beta) 0 sin(beta); 0 1 0; -sin(beta) 0 cos(beta)];
    Rz=[cos(gamma) -sin(gamma) 0; sin(gamma) cos(gamma) 0; 0 0 1];
    Rt=[xx;yy;zz];
    R1=Rx*Ry*Rz;

    a=(R1*a)+Rt;
    b=(R1*b)+Rt;
    c=(R1*c)+Rt;
    d=(R1*d)+Rt;
    e=(R1*e)+Rt;
    f=(R1*f)+Rt;
    g=(R1*g)+Rt;
    h=(R1*h)+Rt;

    cube1=[a.';b.';c.';d.';a.'];
    cube2=[d.';e.';f.';c.';d.'];
    cube3=[e.';g.';h.';f.';e.'];
    cube4=[a.';g.';h.';b.';a.'];
    
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(2,2,2)
    h1 = plot3(cube1(:,1),cube1(:,2),cube1(:,3),'k-')
    hold on
    plot3(cube2(:,1),cube2(:,2),cube2(:,3),'k-')
    hold on
    plot3(cube3(:,1),cube3(:,2),cube3(:,3),'k-')
    hold on
    plot3(cube4(:,1),cube4(:,2),cube4(:,3),'k-')
    axis('equal')
    set(gca,'fontsize',14)

    basepoint=(R1*[0;0;z/2])+Rt;
    plot3(basepoint(1),basepoint(2),basepoint(3),'k.','MarkerSize',15)
    hold on 

    R2=[cos(th(1)) -sin(th(1)) 0;
        sin(th(1)) cos(th(1)) 0;
        0 0 1]*R1;

    joint2=(basepoint-Rt)+[l2*sin(th(2)); 0; (l2*cos(th(2)))];
    J2=(R2*joint2)+Rt;
 
    joint3=[l3*sin(th(2)+th(3)); 0;l3*cos(th(2)+th(3))];
    J3=R2*(joint2+joint3)+Rt;

    joint4=[l4*sin(th(2)+th(3)+th(4)); 0;l4*cos(th(2)+th(3)+th(4))];
    J4=R2*(joint2+joint3+joint4)+Rt;
    
    joint5=[l5*sin(th(2)+th(3)+th(4)+th(5)); 0; l5*cos(th(2)+th(3)+th(4)+th(5))];
    J5=R2*(joint2+joint3+joint4+joint5)+Rt;
    
    plot3(J2(1),J2(2),J2(3),'k.','MarkerSize',15)
    hold on
    plot3(J3(1),J3(2),J3(3),'k.','MarkerSize',15)
    hold on
    plot3(J4(1),J4(2),J4(3),'k.','MarkerSize',15)
    hold on
    plot3(J5(1),J5(2),J5(3),'k.','MarkerSize',15)
    
    i=[basepoint.';J2.'];
    j=[J2.';J3.'];
    k=[J3.';J4.'];
    u=[J4.';J5.'];

    plot3(i(:,1),i(:,2),i(:,3),'k')
    hold on
    plot3(j(:,1),j(:,2),j(:,3),'k')
    hold on
    plot3(k(:,1),k(:,2),k(:,3),'k')
    hold on
    plot3(u(:,1),u(:,2),u(:,3),'k')
    
    %define the final base position
    angend1=ang.data(end,:)
    alpha=angend1(1);
    beta=angend1(2);
    gamma=angend1(3);

    linend1=lin.data(end,:)
    xx=linend1(1);
    yy=linend1(2);
    zz=linend1(3);

    th=theta.data(end,:);

    Rx=[1 0 0;0 cos(alpha) -sin(alpha); 0 sin(alpha) cos(alpha)];
    Ry=[cos(beta) 0 sin(beta); 0 1 0; -sin(beta) 0 cos(beta)];
    Rz=[cos(gamma) -sin(gamma) 0; sin(gamma) cos(gamma) 0; 0 0 1];
    Rt=[xx;yy;zz];
    R1=Rx*Ry*Rz;

    a=[-x/2;y/2;z/2];
    b=[-x/2;y/2;-z/2];
    c=[x/2;y/2;-z/2];
    d=[x/2;y/2;z/2];
    e=[(x/2);(-y/2);(z/2)];
    f=[(x/2);(-y/2);(-z/2)];
    g=[(-x/2);(-y/2);(z/2)];
    h=[(-x/2);(-y/2);(-z/2)];

    a=(R1*a)+Rt;
    b=(R1*b)+Rt;
    c=(R1*c)+Rt;
    d=(R1*d)+Rt;
    e=(R1*e)+Rt;
    f=(R1*f)+Rt;
    g=(R1*g)+Rt;
    h=(R1*h)+Rt;

    cube1=[a.';b.';c.';d.';a.'];
    cube2=[d.';e.';f.';c.';d.'];
    cube3=[e.';g.';h.';f.';e.'];
    cube4=[a.';g.';h.';b.';a.'];

    plot3(cube1(:,1),cube1(:,2),cube1(:,3),'b-')
    hold on
    plot3(cube2(:,1),cube2(:,2),cube2(:,3),'b-')
    hold on
    plot3(cube3(:,1),cube3(:,2),cube3(:,3),'b-')
    hold on
    plot3(cube4(:,1),cube4(:,2),cube4(:,3),'b-')
    xlabel('y axis (inertial frame)','fontsize',12)
    ylabel('x axis (inertial frame)','fontsize',12)
    zlabel('z axis (inertial frame)','fontsize',12)
    
    th=theta.data(end,:);
    
    basepoint=(R1*[0;0;z/2])+Rt;
    plot3(basepoint(1),basepoint(2),basepoint(3),'k.','MarkerSize',15)
    hold on 
    
    R2=[cos(th(1)) -sin(th(1)) 0;
    sin(th(1)) cos(th(1)) 0;
    0 0 1]*R1;
    
    joint2=(basepoint-Rt)+[l2*sin(th(2)); 0; (l2*cos(th(2)))];
    J2=(R2*joint2)+Rt;
 
    joint3=[l3*sin(th(2)+th(3)); 0;l3*cos(th(2)+th(3))];
    J3=R2*(joint2+joint3)+Rt;
    
    joint4=[l4*sin(th(2)+th(3)+th(4)); 0;l4*cos(th(2)+th(3)+th(4))];
    J4=R2*(joint2+joint3+joint4)+Rt;

    joint5=[l5*sin(th(2)+th(3)+th(4)+th(5)); 0; l5*cos(th(2)+th(3)+th(4)+th(5))];
    J5=R2*(joint2+joint3+joint4+joint5)+Rt;
    
    plot3(J2(1),J2(2),J2(3),'b.','MarkerSize',15)
    hold on
    plot3(J3(1),J3(2),J3(3),'b.','MarkerSize',15)
    hold on
    plot3(J4(1),J4(2),J4(3),'b.','MarkerSize',15)
    hold on
    plot3(J5(1),J5(2),J5(3),'b.','MarkerSize',15)

    i=[basepoint.';J2.'];
    j=[J2.';J3.'];
    k=[J3.';J4.'];
    u=[J4.';J5.'];

    plot3(x_a.data, y_a.data, z_a.data,'b--')
    hold on
    plot3(x_d.data, y_d.data, z_d.data,'k--')
    hold on
    
    h2 = plot3(i(:,1),i(:,2),i(:,3),'b')
    hold on
    plot3(j(:,1),j(:,2),j(:,3),'b')
    hold on
    plot3(k(:,1),k(:,2),k(:,3),'b')
    hold on
    plot3(u(:,1),u(:,2),u(:,3),'b')
    
    grid on
    title('Pre and Post Manvouver Position','fontsize',16)
    legend([h1 h2],{'Original Position','Final Position'},'fontsize',12)
    
    figure(1)
    subplot(2,2,3)
    [Xx,Yy,Zz] = workspace_plot6(X);
    plot3(Xx, Yy, Zz, '.')
    title('Workspace Representation','fontsize',16)
    xlabel('y axis (inertial frame)','fontsize',12)
    ylabel('x axis (inertial frame)','fontsize',12)
    zlabel('z axis (inertial frame)','fontsize',12)
    grid on
    hold on
    
    figure(1)
    subplot(2,2,4)
    armconfig6(basedimensions(1:3), X)
    title('Arm Schematic','fontsize',16)

    % outputs important parameters in table
    x = nan;
    h={'Link Lengths (m)(1,2,3,4,end effector)' 'Optimal formfactor' 'Base Dimensions (m)' 'Base Mass(kg) (wet)' 'Base Mass (kg) (dry)' 'Fuel Consumption: per manouver (kg)' 'Power Consumption: per manouver (W)'}
    data = [X(2) X(3) X(4) X(5) X(6);
    basedimensions(5) x x x x; 
    basedimensions(1) basedimensions(2) basedimensions(3) x x;
    basedimensions(6) x x x x; 
    basedimensions(6)-1.5 x x x x;
    F x x x x;
    P_subsystems x x x x;]
    t=uitable(figure(1),'Position',[100 450 700 180],'rowname',h,'data',data)
    %txt_title = uicontrol('Style', 'text', 'Position', [350 450 200 50], 'String', 'Space Robot Parameters','fontsize',16);
    pause(2)
    set(t,'ColumnWidth',{80})
    
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(3,2,1)
    plot(abs(forces.data(:,7:end)))
    xlim([0,200])
    title('Torque Required at Each Joint')
    legend('Joint 1','Joint 2','Joint 3','Joint 4','Joint 5','Joint 6') 
    xlabel('Time')
    ylabel('Torque (Nm)')
    
    subplot(3,2,3)
    plot(power.data(:,7:end))
    xlim([0,200])
    title('Mechanical Power Required at Each Joint')
    xlabel('Time')
    ylabel('Power (W)')
    legend('Joint 1','Joint 2','Joint 3','Joint 4','Joint 5','Joint 6') 
    hold on
 
    subplot(3,2,5)
    plot(power.data(:,7:end)/0.6)
    xlim([0,200])
    title('Electrical Power Required at Each Joint (assuming 60% motor efficiency)')
    xlabel('Time')
    ylabel('Power (W)')
    legend('Joint 1','Joint 2','Joint 3','Joint 4','Joint 5','Joint 6') 
    hold on
    
    subplot(3,2,6)
    plot(theta.data)
    xlim([0,200])
    title('Joint Position Throughout Manouver')
    xlabel('Time')
    ylabel('Position (rad)')
    legend('Joint 1','Joint 2','Joint 3','Joint 4','Joint 5','Joint 6') 
    hold on
    
    x = nan;
    
    h={'Peak Torque (Nm)' 'Average Torque (Nm)' 'Weight (kg) <'}
    data = [max(max(abs(forces.data(:,7:end)))); mean(mean(abs(forces.data(:,7:end)))); 1]
    t=uitable(figure(2),'Position',[900 550 300 100],'rowname',h,'data',data)
    %txt_title = uicontrol('Style', 'text', 'Position', [1150 760 200 30], 'String', 'Motor Requirements','fontsize',16);
    pause(2)
    set(t,'ColumnWidth',{80})
    
    h={'Workspace Area (m^2)' 'Reach (m)' 'Max Payload Dimension (m)' 'Link Diameter: inner(m)','Link Diameter: outer(m)'}
    [area] = workspacearea(X,n)
    data = [area; X(1)+X(2)+X(3)+X(4)+X(5)+X(6); 0.8; 0.03; 0.02]
    t=uitable(figure(2),'Position',[900 350 400 150],'rowname',h,'data',data)
    %txt_title = uicontrol('Style', 'text', 'Position', [900 400 100 30], 'String', 'Arm Spec','fontsize',16);
    pause(2)
    set(t,'ColumnWidth',{80})
    
    for i=1:n-1
        if theta.data(195,i)-theta.data(200,i)>0.0005
           annotation('textbox', [0.6, 0.4, 0.1, 0.1], 'String', "System is unstable in this configuration")
        m = 0
        end
    end
    
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(3,2,1)
    plot(lin.data)
    xlim([0,200])
    title('Linear Position of Base','fontsize',12)
    xlabel('Time')
    ylabel('Linear Position (m)')
    legend('x axis','y axis','z axis') 
    
    subplot(3,2,2)
    plot(ang.data)
    xlim([0,200])
    title('Angular Position of Base','fontsize',12)
    xlabel('Time')
    ylabel('Angular Position (rad)')
    legend('x axis','y axis','z axis')
    
    subplot(3,2,3)
    plot(forces.data(:,1:3))
    xlim([0,200])
    title('Force From Thrusters','fontsize',12)
    xlabel('Time')
    ylabel('Linear Force (N)')
    legend('x axis','y axis','z axis') 

    subplot(3,2,4)
    plot(forces.data(:,4:6))
    xlim([0,200])
    title('Torque From Reaction Wheels','fontsize',12)
    xlabel('Time')
    ylabel('Torque (Nm)')
    legend('x axis','y axis','z axis')
    
    subplot(3,2,5)
    plot(vv)
    xlim([0,200])
    title('Reaction Wheel Velocity','fontsize',12)
    xlabel('Time (s)')
    ylabel('Speed of Reaction Wheel (R.P.M)')
    legend('x axis','y axis','z axis')
    
    if RW == 1
        annotation('textbox', [0.15, 0.15, 0.1, 0.1], 'String', "RW used is Clyde Space small sat reaction wheel")
    elseif RW == 2
        annotation('textbox', [0.15, 0.15, 0.1, 0.1], 'String', "RW used is Collins aerospace reaction wheel")
    end
    
    subplot(3,2,6)
    bar(abs(forces.data(:,1:3)))
    xlim([0,200])
    title('Thruster Firing Pattern','fontsize',12)
    xlabel('Time (s)')
    ylabel('Mass of Fuel Ejected (kg)')
    legend('x axis','y axis','z axis')
    

    
    
end


end
