# Ta-DAH
This is the code for Ta-DAH: task driven automated hardware design of free flying space robots. It is used to design the manipulator configuration and base spacecraft sizing. 
The arm is optimised using a multi-objective cost function and the base spacecraft follows the form factor convention. 


## Getting started

This project works with MATLAB - download all the files in the repo and run newapp.m.
This will generate the interface for the design tool box.
The toolbox can be run with different DOF and final configurations.
Ensure that the final system position matches the DOF of the system. 

Work is presented in:
[1] Lucy Jackson, Celyn Walters, Chakravarthini M. Rai, Steve Eckersley, and Simon Had- field. Ta-DAH: Task driven automated hardware design of free-flying space robots. In Proceedings of the 16th International Conference on Space Robotics and Automation (ICSRA), 19th - 20th July, 2022. Virtual.