function [X,Y,Z] = workspace_plot4(X)

a1=X(1);
a2=X(2);
a3=X(3);
a4=X(4);

alpha1=0;
alpha2=-pi/2;
alpha3=0;
alpha4=-pi/2;

d1=0;
d2=0;
d3=0.044;
d4=0;

t1_min=-pi/2;
t1_max=pi/2;
t2_min=-0.9;
t2_max=0.9;
t3_min=-0.9;
t3_max=0.9;
t4_min=-0.9;
t4_max=0.9;


N = 2000;
t1 = t1_min + (t1_max-t1_min)*rand(N,1);
t2 = t2_min + (t2_max-t2_min)*rand(N,1);
t3 = t3_min + (t3_max-t3_min)*rand(N,1);
t4 = t4_min + (t4_max-t4_min)*rand(N,1);

for i = 1:N
i;
A1 = TranMat(a1,alpha1,d1,t1(i));
A2 = TranMat(a2,alpha2,d2,t2(i));
A3 = TranMat(a3,alpha3,d3,t3(i));
A4 = TranMat(a4,alpha4,d4,t4(i));
T = A1*A2*A3*A4;
X(i)=T(1,4);
Y(i)=T(2,4);
Z(i)=T(3,4);

end


end
