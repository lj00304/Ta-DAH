function [X, F, basedimensions, Operatingtime, P_forformfactor, forces, lin, ang, theta, vv, RW, P_subsystems, x_a, y_a, z_a, x_d, y_d, z_d, power] = freeflying4(desiredreach, payloadmass, n, reacherror, qout)
% final joint location was set by the exhaustive search
qf = qout(7:10)
qf_base = qout(1:6)
% base mass is independant to arm optimistaion
basemass = 1000;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ARM OPTIMISATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% set the range of link lengths based on desired reach and the error. 
X2 = (ceil(desiredreach*10/2))/10; 
X3 = (desiredreach-X2)/2;
X4 = (desiredreach-X2)/2;

%%%%% X4 is 0.05 in the conference paper
X = [0 X2 X3 X4];

% this function produces the cost function at pre decided data points. 
[output,mass,l,b,s,power] = data_points_free_flying_4 (n, X, qf, qf_base, basemass, payloadmass, reacherror);

% Calculates the normalised cost function
norm = max(abs(output(:,[5:10])));
new_output = output(:,[5:10])./norm;

%selects lowest cost function
output(:,end+1) = sum(new_output(:,[5:end]),2);
ordered_rows = sortrows(output,11);

indx=1;
X = ordered_rows(indx,[1:4]);
while sum(X) == 0;
    indx=indx+1;
    X = ordered_rows(indx,[1:4]);
end 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BASE OPTIMISATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% sets the mass and dimensions of different form factors 
% this mass includes the wet mass of a thruster
% analysis starts at 12U from earlier work 
dimensions = {};
dimensions{1} = [0.3 0.2 0.2 3 12 17.5];
dimensions{2} = [0.3 0.3 0.2 4 18 25.4];
dimensions{3} = [0.3 0.4 0.2 5 241 33.4];
dimensions{4} = [0.3 0.3 0.3 6 271 37.4];
dimensions{5} = [0.3 0.4 0.2 7 242 43.4];
dimensions{6} = [0.3 0.3 0.3 8 272 47.5];  
% dimensions{7} = [0.4 0.4 0.4 9 64 65];  
% dimensions{9} = [0.5 0.5 0.5 10 125 85];  
% dimensions{10} = [0.6 0.6 0.6 11 216 100];  

basecomparison = zeros(6,4);

% while loop ensures that the reaction wheel speed does not exceed what is
% allowed. 
k = 1;

while k < 6;
	try
		basedimensions = dimensions{k}(1:3);
		rho=2710;
		radius=[0.03 0.02];
		basemass = dimensions{k}(6);
		ee=[0.0378 0.0376 0.0376; 
			0.0376 0.0379 0.0376; 
			0.0376 0.0376 0.038];
		linklengths=X;
		i=1;
		rho=2710;
		radius=[0.03 0.02];
		mass=zeros(1,n+1);

		%determine the mass, l, s, and b matrix based on desired link lengths. 
			for i=1:n;
				mass(1,i+1)=(rho*pi*(0.03^2)*X(i))-(rho*pi*(0.02^2)*X(i));
			end
			mass(1,1)=basemass;

			for i=1:n;
				l(i+1,:)=[X(i) 0 0];
			end
			l(1,:)=[0 0 0];

			for i=1:n;
				s(i+1,:)=[X(i)/2 0 0];
			end
			s(1,:)=[0 0 basedimensions(1)/2];

			for i=1:n;
				b(i+1,:)=[X(i)/2 0 0];
			end
			b(1,:)=[0 0 0];

	% runs the simulink model with the selected base size depending on if the
	% system is pre or post stabilised. 
	options = simset('SrcWorkspace','current');
	T = sim('dynamicmodel4opp_prestabilized',[],options);
	set_param('dynamicmodel4opp_prestabilized','AlgebraicLoopSolver','LineSearch');
	warning off

	if k < 5;
	% sets the parameters of the smallest RW
	% RW 1 = Satbus4R
			wi = 0.0059;
			wmax = 6500;
			RW = 1;
			rwp = 3;
	else;
	% parameters are set for the 2nd choice reaction wheel, including the
	% effect of the base. Honeybee
		wi = 0.112;
		wmax = 12000;
		RW = 2;
		rwp = 6;
	end
	% angular acceleration of the wheel at each time step is determined. 
	angaccx = forces.data(:,4)/wi;
	angaccy = forces.data(:,5)/wi;
	angaccz = forces.data(:,6)/wi;

	%totalacc = sum(angaccx) + sum(angaccy) + sum(angaccz);
	% for loops carry out integration of the angular acceleration
	vi = 0;
	TT = max(size(angaccx)); 
	for t = 1:TT;
		vx(t,:)=vi;
		vf = abs(angaccx(t))+vi;
		vi = vf;
	end

	vi = 0;
	for t = 1:TT;
		vy(t,:)=vi;
		vf = abs(angaccy(t))+vi;
		vi = vf;
	end

	vi = 0;
	for t = 1:TT;
		vz(t,:)=vi;
		vf = abs(angaccz(t))+vi;
		vi = vf;
	end

	% converts speeds from rad/s to R.P.M
	vx = vx*9.54;
	vy = vy*9.54;
	vz = vz*9.54;

		vv = [vx vy vz];

	% while loop is left if all of the wheel velocity is under the
	% prescribed max. 
    if max(abs(vx)) > wmax  | max(abs(vy)) > wmax | max(abs(vz)) > wmax
        kk = k;
        k = k+1;
    else
        % this means that the system runs and the correct reaction
        % wheel is being used. 
        basedimensions = dimensions{k};
        k = 10 
    end

	catch;
		k = k+1;
	end
end

try 
	formfactorindex = basedimensions(4);
catch
	basedimensions = dimensions{k};
end

% fuel consumption is worked out assuming that the base is kept stable
% throughout operation. 
Isp = 40;
[F,ff] = fuelconsumption(forces,Isp,TT);
% Check takes place to ensure that the system has enough power to perform
% 1 manouvers per orbit.
formfactorindex = basedimensions(4);
formfactor = basedimensions(5);
P = poweratalt_f(35786);
P_forformfactor = P(formfactorindex);
arm_power = sum(power.data(:,7:end)/0.6);
bb = sum(abs(forces.data(3:6)));
base_power = (bb)/0.014;

%this total power usage is calculated taking into consideration the other
%subsystems. 
P_subsystems = ((sum(arm_power) + base_power))+(2.6+3+0.4);
t2 = P_forformfactor-P_subsystems;

% if power is not sufficient a large base spacecraft is selected and the
% fuel and power calculations are carried out again
while t2 < 0
	basedimensions = dimensions{k};
	basemass = dimensions{k}(6);
	options = simset('SrcWorkspace','current');
	T = sim('dynamicmodel4opp_prestabilized',[],options);
	set_param('dynamicmodel4opp_prestabilized','AlgebraicLoopSolver','LineSearch');
	warning off
	[F,ff] = fuelconsumption(forces,Isp,TT);
	formfactorindex = basedimensions(4);
	formfactor = basedimensions(5);
	P = poweratalt_f(500);
	P_forformfactor = P(formfactorindex);
	arm_power = sum(power.data(:,7:end)/0.6);
	bb = sum(abs(forces.data(3:6)));
	base_power = (bb)/0.014;
	P_subsystems = ((sum(arm_power) + base_power))*100+(2.6+3+0.4);
	t2 = P_forformfactor-P_subsystems ;
	kk = kk+1
end

% number of trajectories given fuel mass is calculated.
Operatingtime = (754 - 300)/F;
end

