function armconfig5(basedimensions, l)
%input base spacecraft parameters
height=basedimensions(1); % z axis
width=basedimensions(2); %x axis
depth=basedimensions(3); %y axis

s=[0;0;height/2];


d=[0;0;0.1;-0.1;0];
alpha=[0;pi/2;0;0;pi/2];
v=0.02; %defines joint distance in the y axis - this does not add to the link length
jw=0.1; %defines the link width

%define link limits
t1_min=deg2rad(-90);
t1_max=deg2rad(90);
t2_min=deg2rad(-50);
t2_max=deg2rad(50);
t3_min=deg2rad(-50);
t3_max=deg2rad(50);
t4_min=deg2rad(-50);
t4_max=deg2rad(50);
t5_min=deg2rad(-50);
t5_max=deg2rad(50);

%draw schematic based on the robotic parameters
if l(1)==0
y=0.1;
else
    y=l(1)
end
x=0.1;

if alpha(1)==0      
c=0;
C=0.05;
else
c=-0.05;
C=0;
end
f1a=0;
g1a=0+s(3)+0.05;
f1b=[f1a;f1a+c];
g1b=[g1a;g1a+C];

if alpha(2)==0
c=c;
C=C;
else
h=c;
e=C;
c=e;
C=h;
end
f2a=f1a+d(1);
g2a=g1a+l(1);
f2b=[(f2a);(f2a+c)];
g2b=[g2a;(g2a+C)];

if alpha(3)==0
c=c;
C=C;
else
h=c;
e=C;
c=e;
C=h;
end
f3a=f2a+d(2);
g3a=g2a+l(2);
f3b=[f3a;f3a+c];
g3b=[g3a;g3a+C];

if alpha(4)==0
c=c;
C=C;
else
h=c;
e=C;
c=e;
C=h;
end
f4a=f3a+d(3);
g4a=g3a+l(3);
f4b=[f4a;f4a+c];
g4b=[g4a;g4a+C];

if alpha(5)==0
c=c;
C=C;
else
h=c;
e=C;
c=e;
C=h;
end
f5a=f4a+d(4);
g5a=g4a+l(4);
f5b=[f5a;f5a+c];
g5b=[g5a;g5a+C];

if d(2)==0
    k=0;
else
    k=v;
end

if d(3)==0
    g=0;
else
    g=v;
end

if d(4)==0
    f=0;
else
    f=v;
end

figure(1)
rectangle('Position',[-width/2 -height/2 width height])
hold on
if l(1)==0
rectangle('Position',[-jw/2 s(3) jw y/2+k],'Curvature',0.2);
hold on
text(-0.5,g1a,'Joint 1','fontsize',16)
else
rectangle('Position',[-jw/2+s(1) s(3) jw 0.05+k],'Curvature',0.2);
hold on
text(-0.4,g1a,'Joint 1','fontsize',16) 
hold on
rectangle('Position',[(-jw/2)+d(1) s(3)+0.05+k jw l(1)],'Curvature',0.2);
end
hold on
rectangle('Position',[(d(1)+d(2)-(jw/2)) l(1)+s(3)+0.05-k jw l(2)+g+k],'Curvature',0.2);
hold on
text(-0.3,g2a,'Joint 2','fontsize',16)
hold on
rectangle('Position',[(d(1)+d(2)+d(3)-(jw/2)) (l(1)+l(2)-g+s(3)+0.05) jw l(3)+(2*g)],'Curvature',0.2);
hold on
text(-0.3,g3a,'Joint 3','fontsize',16)
hold on 
rectangle('Position',[(d(1)+d(2)+d(3)+d(4)-(jw/2)) (l(1)+l(2)+l(3)+s(3)+0.05-f) jw l(4)+(2*f)],'Curvature',0.2);
text(-0.3,g4a,'Joint 4','fontsize',16)
hold on
text(-0.3,g5a,'Joint 5','fontsize',16)
hold on
rectangle('Position',[d(1)+d(2)+d(3)+d(4)-(jw/2)-0.05 l(1)+l(2)+l(3)+l(4)+f+s(3)+0.05 0.1+jw 0.05],'Curvature',0.2);
text(0.2,g5a+0.05,'End Effector','fontsize',16)
axis equal

plot([0;0],[0;0.1],'r-','linewidth',2)
plot([0;-0.1],[0;0],'b-','linewidth',2)
plot(0,0,'ko','linewidth',2)
plot(f1a,g1a,'ko','linewidth',2)
plot(f1b,g1b,'r-','linewidth',2)
plot(f2a,g2a,'ko','linewidth',2)
plot(f2b,g2b,'r-','linewidth',2)
plot(f3a,g3a,'ko','linewidth',2)
plot(f3b,g3b,'r-','linewidth',2)
plot(f4a,g4a,'ko','linewidth',2)
plot(f4b,g4b,'r-','linewidth',2)
plot(f5a,g5a,'ko','linewidth',2)
plot(f5b,g5b,'r-','linewidth',2)
legend({'z axis','x axis','Joint Center'},'fontsize',16)
% title('Manipulator Schematic')
xlabel('X axis (Inertial Frame)','fontsize',20)
ylabel('Z axis (Inertial Frame)','fontsize',20)

end
