function [area] = workspacearea(X,n)

thetamax=0.9;

if n==4
    a1 = (X(3)^2)*thetamax;
    a2 = ((X(3)+X(2))^2)*thetamax;
    area = a1 + a2;
end
    
   
if n==5
    a1 = (X(4)^2)*thetamax;
    a2 = ((X(4)+X(3))^2)*thetamax;
    a3 = ((X(4)+X(3)+X(2))^2)*thetamax;
    area = a1 + a2 + a3;
end

if n==6
    a1 = (X(5)^2)*thetamax;
    a2 = ((X(5)+X(4))^2)*thetamax;
    a3 = ((X(5)+X(4)+X(3))^2)*thetamax;
    a4 = ((X(5)+X(4)+X(3)+X(2))^2)*thetamax;
    area = a1 + a2 + a3 +a4;
end

end