
function [man]=manipulability4(theta,n,s,b)
phi=[0;0;0];
[Rw,Base_Rot,Rot_b,Rot_b_B] = Rotation_Matrices_4(phi,theta);
[Jv_m,Jw_m] = Jacobian_Matrices_For_Lucy4(n,s,b,theta,Rot_b_B,Rw,Base_Rot);

for j=1:n
J_m=[Jv_m{j} Jw_m{j}];
J_mt=transpose(J_m);
end

man=sqrt(det(J_m*J_mt));
end



